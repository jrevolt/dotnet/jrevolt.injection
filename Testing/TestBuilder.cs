﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Autofac;
using Autofac.Core;
using Autofac.Extras.Moq;
using Moq;

namespace JRevolt.Injection.Testing
{
   public class TestBuilder
   {
      private readonly MockRepository repo = new(MockBehavior.Loose);

      //private readonly MetadataBuilder metadata;
      private readonly List<Assembly> assemblies = new();
      private readonly List<Type> components = new();
      private readonly List<Mock> mocks = new();
      private readonly List<Action<ContainerBuilder>> builders = new();


      public TestBuilder() => Expression.Empty();

      //public TestBuilder(Metadata metadata) => this.metadata = metadata;

      public TestBuilder WithAssemblies(params Assembly[] assemblies)
      {
         this.assemblies.AddRange(assemblies);
         return this;
      }

      public TestBuilder WithComponent<T>()
      {
         components.Add(typeof(T));
         return this;
      }

      public TestBuilder WithMock<T>(Action<Mock<T>>? setup = null) where T : class
      {
         var mock = repo.Create<T>(MockBehavior.Loose);
         setup?.Invoke(mock);
         mocks.Add(mock);
         return this;
      }

      public TestBuilder WithBuilderAction(Action<ContainerBuilder> action)
      {
         builders.Add(action);
         return this;
      }

      public TestContext Build(object? test = null)
      {
         var ctx = new TestContext(this);
         if (test == null) return ctx;

         var meta = new MetadataBuilder().UseType(test.GetType()).Build();
         meta.GetInjections(test.GetType()).ForEach(member =>
         {
            var inject = member.GetCustomAttribute<InjectAttribute>();
            Debug.Assert(inject != null);
            var memberType = member.GetMemberType();
            var service = inject.Name != null ? (Service) new KeyedService(inject.Name, memberType) : new TypedService(memberType);
            var resolved = ctx.ResolveService(service);
            meta.ResolveMemberWriter(member).SetValue(test, resolved);
         });
         return ctx;
      }

      internal AutoMock BuildInternal()
      {
         return AutoMock.GetLoose(builder =>
         {
            var meta = new Metadata(assemblies, components);
            builder.UseInjector(meta);
            mocks.ForEach(x => builder.RegisterInstance(x.Object)
               .AsSelf()
               .AsImplementedInterfaces()
               .SingleInstance()
            );
            builders.ForEach(x => x(builder));
         });
      }

      public void Run<T>(Action<T> action) where T : class => Run(scope => action(scope.Resolve<T>()));

      public void Run<T>(Action<ILifetimeScope,T> action) where T : class => Run(scope => action(scope, scope.Resolve<T>()));

      public void Run(Action<ILifetimeScope> action)
      {
         using var mock = BuildInternal();
         using var container = mock.Container;
         using var scope = container.BeginLifetimeScope();
         action(scope);
      }
   }

   internal static class InternalExtensions
   {
      internal static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
      {
         foreach (var x in enumerable) action(x);
      }
   }
}
