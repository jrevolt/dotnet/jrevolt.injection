using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Autofac;
using Autofac.Core;
using Autofac.Core.Lifetime;
using Autofac.Core.Resolving;
using Autofac.Extras.Moq;

namespace JRevolt.Injection.Testing
{
   public class TestContext : ILifetimeScope
   {
      public TestContext(TestBuilder builder)
      {
         Mock = builder.BuildInternal();
         Container = Mock.Container;
         Scope = Container.BeginLifetimeScope();
      }

      internal AutoMock Mock { get; }
      internal IContainer Container { get; }
      internal ILifetimeScope Scope { get; }

      public IComponentRegistry ComponentRegistry => Scope.ComponentRegistry;

      public IDisposer Disposer => Scope.Disposer;

      public object Tag => Scope.Tag;

      public object ResolveComponent(ResolveRequest request)
         => Scope.ResolveComponent(request);

      public void Dispose()
      {
         Dispose(true);
         GC.SuppressFinalize(this);
      }

      protected virtual void Dispose(bool disposing)
      {
         Scope.Dispose();
         Container.Dispose();
         Mock.Dispose();
      }



      public async ValueTask DisposeAsync()
      {
         await Scope.DisposeAsync();
         await Container.DisposeAsync();
         Mock.Dispose();
      }

      public ILifetimeScope BeginLifetimeScope()
         => Scope.BeginLifetimeScope();

      public ILifetimeScope BeginLifetimeScope(object tag)
         => Scope.BeginLifetimeScope(tag);

      public ILifetimeScope BeginLifetimeScope(Action<ContainerBuilder> configurationAction)
         => Scope.BeginLifetimeScope(configurationAction);

      public ILifetimeScope BeginLifetimeScope(object tag, Action<ContainerBuilder> configurationAction)
         => Scope.BeginLifetimeScope(tag, configurationAction);

      [ExcludeFromCodeCoverage]
      public event EventHandler<LifetimeScopeBeginningEventArgs> ChildLifetimeScopeBeginning
      {
         add => Scope.ChildLifetimeScopeBeginning += value;
         remove => Scope.ChildLifetimeScopeBeginning -= value;
      }

      [ExcludeFromCodeCoverage]
      public event EventHandler<LifetimeScopeEndingEventArgs> CurrentScopeEnding
      {
         add => Scope.CurrentScopeEnding += value;
         remove => Scope.CurrentScopeEnding -= value;
      }

      [ExcludeFromCodeCoverage]
      public event EventHandler<ResolveOperationBeginningEventArgs> ResolveOperationBeginning
      {
         add => Scope.ResolveOperationBeginning += value;
         remove => Scope.ResolveOperationBeginning -= value;
      }
   }
}
