using System;
using System.Linq.Expressions;
using Autofac.AttributeExtensions;
using JetBrains.Annotations;

namespace JRevolt.Injection.Testing
{
   [AttributeUsage(AttributeTargets.Property|AttributeTargets.Class)]
   [MeansImplicitUse]
   public class EnableInjectionAttribute : RegistrationAttribute
   {
      public EnableInjectionAttribute() : base(Lifestyle.InstancePerLifetimeScope) => Expression.Empty();
   }

   public static class TestingExtensions
   {
   }
}
