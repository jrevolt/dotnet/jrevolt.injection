using System;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;

namespace JRevolt.Injection.AspNetCore
{
   public class InjectorServiceProviderFactory : IServiceProviderFactory<ContainerBuilder>
   {
      private readonly Metadata metadata;
      private readonly Injector.ProcessRegistration? onRegistration;

      public InjectorServiceProviderFactory(Metadata? metadata = null, Injector.ProcessRegistration? onRegistration = null)
      {
         this.onRegistration = onRegistration;
         this.metadata = metadata ?? new Metadata(Assembly.GetCallingAssembly());
      }

      public ContainerBuilder CreateBuilder(IServiceCollection services)
      {
         services.AddSingleton<IServiceScopeFactory, RequestScopeFactory>();
         var builder = new ContainerBuilder();
         builder.Populate(services);
         builder.UseInjector(metadata, onRegistration);
         return builder;
      }

      public IServiceProvider CreateServiceProvider(ContainerBuilder containerBuilder)
      {
         var container = containerBuilder.Build();
         return new AutofacServiceProvider(container);
      }
   }
}
