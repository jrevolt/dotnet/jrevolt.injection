using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace JRevolt.Injection.AspNetCore
{
   /// <summary>
   ///
   /// </summary>
   public static class Extensions
   {

      public static void AddInjectorServices(this IServiceCollection services)
      {
         services
            .AddControllers()
            .ConfigureApplicationPartManager(manager => manager.FeatureProviders.Add(new InjectorControllerFeatureProvider()))
            .AddControllersAsServices();
      }

      public static void UseInjector(this IApplicationBuilder application)
      {
         application
            .UseRouting()
            .UseEndpoints(endpoints => endpoints.MapControllers());
      }


   }
}
