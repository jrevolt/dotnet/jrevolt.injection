using System;
using Autofac.AttributeExtensions;
using JetBrains.Annotations;

namespace JRevolt.Injection.AspNetCore
{
   [AttributeUsage(AttributeTargets.Class, AllowMultiple = true), UsedImplicitly]
   public class ControllerScopeAttribute : SingleInstanceAttribute
   {
   }

   [AttributeUsage(AttributeTargets.Class, AllowMultiple = true), UsedImplicitly]
   public class ServiceScopeAttribute : SingleInstanceAttribute
   {
   }

   [AttributeUsage(AttributeTargets.Class, AllowMultiple = true), UsedImplicitly]
   public class RequestScopeAttribute : InstancePerLifetimeScopeAttribute
   {
   }

}
