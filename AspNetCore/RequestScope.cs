using System;
using Autofac;
using Autofac.Core.Lifetime;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;

namespace JRevolt.Injection.AspNetCore
{
   internal class RequestScopeFactory : IServiceScopeFactory
   {
      private readonly ILifetimeScope root;

      public RequestScopeFactory(ILifetimeScope root) => this.root = root;

      public IServiceScope CreateScope() => new RequestScope(root.BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag));
   }

   internal sealed class RequestScope : IServiceScope
   {
      public RequestScope(ILifetimeScope scope) => ServiceProvider = new AutofacServiceProvider(scope);

      //[SuppressMessage("", "S3881:")]
      public void Dispose() => (ServiceProvider as IDisposable)?.Dispose();

      public IServiceProvider ServiceProvider { get; }
   }

}
