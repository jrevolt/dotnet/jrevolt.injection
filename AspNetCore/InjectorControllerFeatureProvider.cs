using System.Reflection;
using Autofac.AttributeExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;

namespace JRevolt.Injection.AspNetCore
{
   public class InjectorControllerFeatureProvider : ControllerFeatureProvider
   {
      protected override bool IsController(TypeInfo typeInfo) =>
         typeInfo.GetCustomAttribute<ApiControllerAttribute>() != null &&
         typeInfo.GetCustomAttribute<RegistrationAttribute>() != null;
   }
}
