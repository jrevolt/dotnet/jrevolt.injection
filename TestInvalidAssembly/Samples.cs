using System.Diagnostics.CodeAnalysis;
using Autofac.AttributeExtensions;

namespace JRevolt.Injection.TestInvalidAssembly
{
   public interface IService
   {
   }

   [ExcludeFromCodeCoverage]
   [SingleInstance]
   public class ReadOnlyAutoPropertyFail
   {
      [Inject]
      private IService Value => null;
   }

   [InstancePerLifetimeScope]
   internal class OnActivatedSample
   {
      [OnActivated]
      protected virtual void OnActivated()
      {
      }
   }

   [InstancePerLifetimeScope]
   internal class OnActivatedSampleExtended : OnActivatedSample
   {
      [OnActivated]
      protected override void OnActivated()
      {
         base.OnActivated();
      }
   }

   [InstancePerLifetimeScope]
   internal class OnActivatedSampleInvalid
   {
      [OnActivated]
      private void OnActivated1()
      {
      }

      [OnActivated]
      private void OnActivated2()
      {
      }
   }

}
