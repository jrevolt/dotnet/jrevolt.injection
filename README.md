# JRevolt Injection

- assembly scanning
- services declared by attribute
- dependencies declared by attribute on field/property
- based on `Autofac`
- registrations controlled by `Autofac.AttributeExtensions`
- status: `working prototype`
- see [wiki](https://gitlab.com/jrevolt/dotnet/jrevolt.injection/wikis/home)

## Example

```c#
interface IMyService
{}

[Service]
class MyService : IMyService 
{}

[Service]
class MyServiceClient 
{
  [Inject]
  protected IMyService service;
}

```
