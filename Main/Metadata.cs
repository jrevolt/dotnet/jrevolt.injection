using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Autofac.AttributeExtensions;
using Castle.Core.Internal;
using Serilog;

namespace JRevolt.Injection
{

   public class MetadataBuilder
   {
      private List<Assembly> Assemblies { get; } = new();
      private List<Type> Types { get; } = new();

      public MetadataBuilder UseAssembly(params Assembly[] assemblies)
      {
         Assemblies.AddRange(assemblies);
         return this;
      }

      public MetadataBuilder UseType(params Type[] types)
      {
         Types.AddRange(types);
         return this;
      }

      public Metadata Build()
      {
         return new(Assemblies, Types);
      }
   }

   /// <summary>
   /// Builds injector metadata by (a) either scanning list of assemblies for types with supported registration attributes,
   /// or (b) explicitly listing selected types (which still must include supported registration attributes).
   ///
   /// Assembly scanning is generally useful for production code while type scanning is more suitable for unit testing.
   ///
   /// </summary>
   public class Metadata
   {
      public static Metadata Empty => new MetadataBuilder().Build();

      private static ILogger Log => Serilog.Log.ForContext<Metadata>();

      private readonly ISet<Assembly> assemblyIndex = new HashSet<Assembly>();
      private readonly ISet<Type> classIndex = new HashSet<Type>();
      private readonly ISet<MemberWrapper> memberIndex = new HashSet<MemberWrapper>();

      private readonly IDictionary<Type, List<MemberInfo>> membersByType = new Dictionary<Type, List<MemberInfo>>();

      private readonly IDictionary<PropertyInfo, FieldInfo> backingFields = new Dictionary<PropertyInfo, FieldInfo>();

      private readonly IDictionary<Type, MethodInfo?> onActivatedIndex = new Dictionary<Type, MethodInfo?>();

      public Metadata(params Assembly[] assemblies) : this(assemblies, Array.Empty<Type>())
      {
      }

      public Metadata(params Type[] types) : this(Array.Empty<Assembly>(), types)
      {
      }

      protected internal Metadata(IEnumerable<Assembly> assemblies, IEnumerable<Type> types)
      {
         Scan(assemblies);
         Scan(types);
      }



      /// <summary>
      /// Scan given assemblies for a list types/classes valid for DI registration
      /// </summary>
      /// <param name="assemblies"></param>
      /// <returns>this metadata instance</returns>
      /// /// <see cref="RegistrationAttribute"/>
      private void Scan(IEnumerable<Assembly> assemblies)
      {
         bool IsNewAssembly(Assembly a) => !assemblyIndex.Contains(a);

         assemblies
            .Where(IsNewAssembly)
            .OrderBy(a => a.GetName().Name)
            .Select(IndexAssembly)
            .ForEach(a => Scan(a.GetTypes()));
      }

      /// <summary>
      /// Scan given types/classes and use them if they are valid for DI registration
      /// </summary>
      /// <param name="types"></param>
      /// <returns>this metadata instance</returns>
      /// <see cref="RegistrationAttribute"/>
      private void Scan(IEnumerable<Type> types)
      {
         static bool IsValidRegistration(Type t) => t.IsClass && t.GetCustomAttributes<RegistrationAttribute>().Any();
         static bool IsValidInject(MemberInfo m) => m.GetCustomAttribute<InjectAttribute>() != null;
         bool IsNewRegistration(Type t) => IsValidRegistration(t) && !classIndex.Contains(t);
         bool IsNewInject(MemberWrapper m) => IsValidInject(m.Member) && !memberIndex.Contains(m);

         types
            .Where(IsNewRegistration)
            .OrderBy(t => t.FullName)
            .Select(IndexClass)
            .ForEach(type => type
               .GetAllDeclaredFieldsAndProperties()
               .Where(IsNewInject)
               .OrderBy(m => m.Member.Name)
               .ForEach(m => IndexMember(type, m))
            );
      }

      private Assembly IndexAssembly(Assembly assembly)
      {
         Log.Debug("Scanning ({0}) {1}", "A", assembly.GetName().Name);
         assemblyIndex.Add(assembly);
         return assembly;
      }

      private Type IndexClass(Type type)
      {
         Log.Debug("Scanning ({0}) {1}", "T", type);
         classIndex.Add(type);
         onActivatedIndex[type] = FindOnActivated(type);
         return type;
      }

      private static MethodInfo? FindOnActivated(IReflect type)
      {
         var candidates = type
            .GetMethods(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public)// NOSONAR S3011 accessibility bypass
            .Where(m => m.GetCustomAttribute<OnActivatedAttribute>() != null)
            .ToList();

         if (candidates.IsNullOrEmpty()) return null;
         if (candidates.Count > 1) throw new ArgumentException($"Multiple [OnActivated] methods for type {type}");

         return candidates.First();
      }

      private void IndexMember(Type master, MemberWrapper memberWrapper)
      {
         var member = memberWrapper.Member;
         var memberType = member.GetMemberType();
         var flag = member switch { FieldInfo => "F", PropertyInfo => "P", _ => "?" };
         Log.Verbose("Scanning ({0}) {1}.{2} : {3}", flag, master.Name, member.Name, memberType.Name);
         memberIndex.Add(memberWrapper);
         if (!membersByType.TryGetValue(master, out var index)) membersByType[master] = index = new List<MemberInfo>();
         index.Add(member);

         if (member is PropertyInfo pi && !pi.CanWrite)
         {
            backingFields[pi] =
               ResolveReadOnlyPropertyBackingField(pi)
               ?? throw new ArgumentNullException(pi.Name,
                  $"{pi.DeclaringType}.{pi.Name} : Property is not writable and backing field cannot be resolved.");
         }
      }

      internal IEnumerable<Type> GetRegisteredTypes() => classIndex;

      internal IEnumerable<MemberInfo> GetInjections(Type type)
         => membersByType.TryGetValue(type, out var result) ? result : Enumerable.Empty<MemberInfo>();

      internal static FieldInfo? ResolveReadOnlyPropertyBackingField(PropertyInfo pi)
      {
         // todo/warning: ugly hack; is there a better way? (related: #9)
         var name = $"<{pi.Name}>k__BackingField";
         Debug.Assert(pi.DeclaringType != null);
         var f = pi.DeclaringType.GetRuntimeFields().FirstOrDefault(x => x.Name.Contains(name));

         // no backing field or different naming
         if (f == null) return null;

         Debug.Assert(f.FieldType == pi.PropertyType);
         return f;
      }

      internal MemberInfo ResolveMemberWriter(MemberInfo member)
      {
         return member switch
         {
            FieldInfo fi => fi,
            PropertyInfo {CanWrite: true} pi => pi,
            PropertyInfo {CanWrite: false} pi => backingFields[pi],
            _ => throw new ArgumentException($"unexpected member type: {member.GetType()}")
         };
      }

      internal MethodInfo? ResolveOnActivated(Type type)
      {
         onActivatedIndex.TryGetValue(type, out var action);
         return action;
      }
   }
}
