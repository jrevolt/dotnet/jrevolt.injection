using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.AttributeExtensions;
using Autofac.Builder;
using Autofac.Core;
using Autofac.Core.Lifetime;

namespace JRevolt.Injection
{
   internal static class InternalExtensions
   {
      internal static T RegisterAs<T>(this T reg, RegistrationAttribute attr, Type type)
         where T : IRegistrationBuilder<object, ConcreteReflectionActivatorData, SingleRegistrationStyle>
      {
         RegisterAsTypes(attr, type).ForEach(asType =>
         {
            RegisterNamed(reg, attr, asType);
            reg.As(asType);
         });
         return reg;
      }

      private static IEnumerable<Type> RegisterAsTypes(RegistrationAttribute attribute, Type type)
      {
         return attribute.As
                ?? (!attribute.AsImplementedInterfaces ? new[] {type} : type.GetTypeInfo().GetInterfaces().Concat(new[] {type}));
      }

      private static void RegisterNamed<T>(this T reg, RegistrationAttribute attribute, Type asType)
         where T : IRegistrationBuilder<object, ConcreteReflectionActivatorData, SingleRegistrationStyle>
      {
         if (attribute.Name != null) reg.Named(attribute.Name, asType);
      }

      internal static T SetLifestyle<T>(this T reg, RegistrationAttribute attr)
         where T : IRegistrationBuilder<object, ConcreteReflectionActivatorData, SingleRegistrationStyle>
      {
         var tags = new[] { MatchingScopeLifetimeTags.RequestLifetimeScopeTag };
         return attr.Lifestyle switch
         {
            Lifestyle.SingleInstance => (T) reg.SingleInstance(),
            Lifestyle.InstancePerDependency => (T) reg.InstancePerDependency(),
            Lifestyle.InstancePerLifetimeScope => attr.LifetimeScopeTags?.Any() == true
               ? (T) reg.InstancePerMatchingLifetimeScope(tags.Concat(attr.LifetimeScopeTags).ToArray())
               : (T) reg.InstancePerLifetimeScope(),
            _ => throw new NotImplementedException($"Unsupported {nameof(attr.Lifestyle)} value: {attr.Lifestyle}")
         };
      }

      internal static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
      {
         foreach (var x in enumerable) action(x);
      }

      internal static Type GetMemberType(this MemberInfo member)
      {
         return member switch
         {
            FieldInfo f => f.FieldType,
            PropertyInfo p => p.PropertyType,
            _ => throw new ArgumentOutOfRangeException(member.GetType().ToString())
         };
      }

      internal static void SetValue(this MemberInfo member, object target, object value)
      {
         switch (member)
         {
            case FieldInfo f:
               f.SetValue(target, value);
               break;
            case PropertyInfo p:
               p.SetValue(target, value);
               break;
            default:
               throw new ArgumentOutOfRangeException(member.GetType().ToString());
         }
      }

      internal static IEnumerable<MemberWrapper> GetAllDeclaredFieldsAndProperties(this Type type)
      {
         static bool IsFieldOrProperty(MemberInfo m) => m is FieldInfo || m is PropertyInfo;

         const BindingFlags flags = BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.NonPublic | BindingFlags.Public; // NOSONAR S3011: accessibility bypass

         var result = new List<MemberWrapper>();
         for (var t = type; t != null; t = t.BaseType)
            result.AddRange(t.GetMembers(flags).Where(IsFieldOrProperty).Select(mi => new MemberWrapper(type, mi)));

         return result;
      }

      internal static string? ToInstanceIdString(this object? instance)
         => instance != null ? $"{instance.GetType()}@{instance.GetHashCode()}" : null;

      internal static string? ToScopeIdString(this ILifetimeScope? scope)
         => scope != null ? $"{scope.Tag}@{scope.GetHashCode()}" : null;

      internal static ILifetimeScope? FindMatchingScope(this IComponentLifetime component, ILifetimeScope mostNestedVisibleScope)
      {
         if (!(mostNestedVisibleScope is ISharingLifetimeScope scope)) return mostNestedVisibleScope;
         if (!(component is MatchingScopeLifetime msl)) return component.FindScope(scope);
         for (var x = scope; x != null; x = x.ParentLifetimeScope)
         {
            if (msl.TagsToMatch.Contains(x.Tag)) return x;
         }
         return null;
      }

   }

   // ReSharper disable once MemberCanBePrivate.Global
   // ReSharper disable once NotAccessedField.Global
   internal class MemberWrapper
   {
      public MemberWrapper(Type owner, MemberInfo member)
      {
         Owner = owner;
         Member = member;
      }

      internal readonly Type Owner;
      internal readonly MemberInfo Member;
   }
}
