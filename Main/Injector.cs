using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using Autofac;
using Autofac.AttributeExtensions;
using Autofac.Builder;
using Autofac.Core;
using Autofac.Core.Resolving;
using Castle.DynamicProxy;
using JetBrains.Annotations;
using Serilog;

namespace JRevolt.Injection
{
   public class Injector
   {
      public Injector(Metadata metadata) => Metadata = metadata;

      private static ILogger Log => Serilog.Log.ForContext<Injector>();

      private abstract class Locking {} // logging sub-namespace
      private static ILogger LockingLog => Serilog.Log.ForContext<Locking>();

      private static readonly AsyncLocal<IContainer> CurrentContainer = new();

      private static readonly AsyncLocal<ILifetimeScope> CurrentScope = new();

      private readonly ProxyGenerator proxyGenerator = new();

      public Metadata Metadata { get; }

      private readonly IDictionary<Type,object> proxies = new Dictionary<Type, object>();

      private readonly ThreadLocal<IComponentRegistration> resolving = new();

      private readonly ThreadLocal<IList<Action>> activations = new(() => new List<Action>());

      public delegate void ProcessRegistration(ContainerBuilder builder, IComponentRegistration registration);

      // ReSharper disable once MemberCanBeMadeStatic.Global
      public void Register(ContainerBuilder builder, ProcessRegistration? onRegistration = null)
      {
         builder.RegisterInstance(this).AsSelf();
         Metadata.GetRegisteredTypes().ForEach(type =>
         {
            var attr = type.GetCustomAttribute<RegistrationAttribute>() ?? new ServiceAttribute();
            var reg = builder
               .RegisterType(type)
               .SetLifestyle(attr)
               .RegisterAs(attr, type)
               .OnActivated(OnActivated)
               .OnRelease(OnRelease)
               .CreateRegistration();
            onRegistration?.Invoke(builder, reg);
         });
      }

      public static IContainer Container => CurrentContainer.Value!;

      /// <summary>
      /// Returns active lifetime scope of the container
      /// </summary>
      public static ILifetimeScope Scope => CurrentScope.Value!;

      internal void OnBeginLifetimeScope(ILifetimeScope scope)
      {
         // if this is container, just remember it and resolve root scope, which it is processed as usual
         if (scope is IContainer container)
         {
            scope = container.Resolve<ILifetimeScope>();
            CurrentContainer.Value = container;
            container.CurrentScopeEnding += (_, _) => CurrentContainer.Value = null!;
         }

         Log.Debug("Begin scope: {0}", scope.ToScopeIdString());

         // register event handlers for new child scopes and disposal of this one
         scope.ChildLifetimeScopeBeginning += (_, args) => OnBeginLifetimeScope(args.LifetimeScope);
         scope.CurrentScopeEnding += (_, args) => OnDisposeScope(args.LifetimeScope);

         // need to identify self-lookups
         scope.ComponentRegistry.TryGetRegistration(new TypedService(typeof(ILifetimeScope)), out var scopeRegistration);

         // hook into events to implement some locking to avoid some lookup/activation issues
         scope.ResolveOperationBeginning += (_, beg) =>
         {
            // ReSharper disable once LocalVariableHidesMember
            // ReSharper disable once InconsistentNaming
            var LockingLog = Injector.LockingLog; // todo: optimize, avoid lookups on every call //NOSONAR
            beg.ResolveOperation.InstanceLookupBeginning += (_, args) =>
            {
               var reg = args.InstanceLookup.ComponentRegistration;

               if (reg.Id == scopeRegistration?.Id) return; // skip all this for ILifetimeScope self-registration

               // remember or resolve current resolution root
               var root = resolving.Value ?? (resolving.Value = reg);
               var isRoot = reg.Id == root.Id;

               var lckname = $"jrevolt.injection:resolve={reg.Id},root={root.Id}";
               var lck = new Mutex(false, lckname);
               LockingLog.Verbose("locking {0} {1}", reg.Activator, lckname);
               var timer = new Stopwatch(); timer.Start();
               if (!lck.WaitOne()) throw new DependencyResolutionException($"Unable to lock {reg.Activator}: {lckname}");
               timer.Stop();
               LockingLog.Verbose("locked {0} {1} in {2} ticks", reg.Activator, lckname, timer.ElapsedTicks);
               timer.Restart();
               beg.ResolveOperation.CurrentOperationEnding += (_, _) =>
               {
                  lck.ReleaseMutex(); // todo review: can this fail, and if yes, can that be ignored?
                  LockingLog.Verbose("unlocked {0} {1} in {2} ticks", reg.Activator, lckname, timer.ElapsedTicks);
                  if (isRoot) root = resolving.Value = null!;
               };

               // locking above prevents following undesired scenario:
               // - new instance is created and shared but not yet activated
               // - concurrent lookups will receive the instance that is still in process of activation/injection
               // - concurrent process is now using partially configured instance, hence results are undefined (typically NPE/NRE)
            };
         };
         CurrentScope.Value = scope;
      }

      private static void OnDisposeScope(ILifetimeScope scope)
      {
         Log.Debug("Closing scope: {0}", scope.ToScopeIdString());
         var current = CurrentScope.Value;
         var parent = (scope as ISharingLifetimeScope)?.ParentLifetimeScope;
         if (current == scope) CurrentScope.Value = parent!;
      }

      private static int GetActivationScopeDepth(IComponentContext? ctx)
      {
         var depth = -1;
         if (!(ctx is ISharingLifetimeScope scope)) return depth;
         for (var x = scope; x != null; x = x.ParentLifetimeScope) depth++;
         return depth;
      }

      private void InjectMembers(object target, IComponentContext ctx, IComponentRegistration registration)
      {
         var Log = Serilog.Log.ForContext(GetType()); //NOSONAR //todo
         var activationScope = ((IInstanceLookup) ctx).ActivationScope;
         var currentScope = Scope!;
         var targetScopeDepth = GetActivationScopeDepth(registration.Lifetime.FindMatchingScope(currentScope));
         Metadata.GetInjections(target.GetType()).ForEach(member =>
         {
            var inject = member.GetCustomAttribute<InjectAttribute>();
            Debug.Assert(inject != null);
            var memberType = member.GetMemberType();

            var service = inject.Name != null ? (Service) new KeyedService(inject.Name, memberType) : new TypedService(memberType);
            currentScope.ComponentRegistry.TryGetRegistration(service, out var dependencyRegistration);

            var drtype = typeof(IDynamicServiceResolver<>).MakeGenericType(memberType);
            currentScope.TryResolve(drtype, out var dresolver);

            var unresolvable = dependencyRegistration == null && dresolver == null;
            if (unresolvable) throw new DependencyResolutionException($"Unresolved: {service}");

            var sourceScopeDepth = GetActivationScopeDepth(dependencyRegistration?.Lifetime.FindMatchingScope(currentScope));
            var isNeedProxy = sourceScopeDepth > targetScopeDepth || sourceScopeDepth == -1 && dresolver == null;

            Log.Verbose("Injecting field {0}.{1}:{2} {3}", target.GetType(), member.Name, memberType.Name, isNeedProxy ? "(proxied)" : "");

            var resolved = Resolve(service, activationScope, memberType, inject, isNeedProxy);

            // todo review this (fallback to IDynamicServiceResolver when no service is found)
            if (resolved == null)
            {
               (dresolver as IDynamicServiceResolver)?.Resolve(target, ctx, registration, member, out resolved);
            }

            if (resolved == null) throw new InjectionException($"Unresolved: {service}");

            Metadata.ResolveMemberWriter(member).SetValue(target, resolved);
         });
      }

      private object? Resolve(Service service, IComponentContext activationScope, Type memberType, InjectAttribute inject, bool isNeedProxy)
      {
         if (!isNeedProxy)
         {
            activationScope.TryResolveService(service, out var resolved);
            return resolved;
         }

         if (proxies.TryGetValue(memberType, out var proxy)) return proxy;


         // this will create proxy for only interface declared on receiver side (injected field)
         // however, actual injected object may have more interfaces, and receiver may try to cast it to desired type
         // this may work if the object is injected as is, but will fail if the proxy is created
         // proxy is typically created when the scope of the target object is wider/bigger/longer than that of injected object
         // there is actually no way how to support this properly here because the injected object and its scope may not even
         // be available at the time of the target creation/injection
         // this is a problem of transparency of the proxies, and the only solution is extract the original object from the proxy
         // and use it as such. While inconvenient, at least this is possible due to extra proxy interfaces

         proxy = memberType.IsInterface
            ? proxyGenerator.CreateInterfaceProxyWithoutTarget(memberType, new ScopedServiceProxy(inject, memberType))
            : proxyGenerator.CreateClassProxy(memberType, new ScopedServiceProxy(inject, memberType));

         proxies.Add(memberType, proxy);

         return proxy;

      }

      [UsedImplicitly]
      public interface IScopedServiceProxy
      {
         Type Type { get; }
         InjectAttribute Inject { get; }
         Service ServiceType { get; }
      }

      internal class ScopedServiceProxy : IInterceptor, IScopedServiceProxy
      {
         public ScopedServiceProxy(InjectAttribute inject, Type type)
         {
            Type = type;
            Inject = inject;
            ServiceType = inject.Name != null ? (Service) new KeyedService(inject.Name, type) : new TypedService(type);
         }

         public Type Type { get; }
         public InjectAttribute Inject { get; }
         public Service ServiceType { get; }

         public void Intercept(IInvocation invocation)
         {
            var Log = Serilog.Log.ForContext(GetType());
            var scope = Scope!;
            var resolved = scope.ResolveOptionalService(ServiceType);
            Log.Verbose("Scoped proxy resolving {0} from scope {1}. Resolved: {2}", ServiceType, scope.ToScopeIdString(), resolved.ToInstanceIdString());
            invocation.ReturnValue  = invocation.Method.Invoke(resolved, invocation.Arguments);
         }
      }

      private void OnActivated(IActivatedEventArgs<object> args)
         => Activate(args.Instance, args.Context, args.Component);

      private void Activate(object obj, IComponentContext ctx, IComponentRegistration reg)
      {
         var isRoot = reg.Id == resolving.Value!.Id;
         var activate = activations.Value!;

         // ReSharper disable once LocalVariableHidesMember
         // ReSharper disable once InconsistentNaming
         var Log = Injector.Log; //NOSONAR //todo qdh to avoid lookups

         try
         {
            Log.Verbose("Activating {0} from scope {1}", obj.ToInstanceIdString(), Scope.ToScopeIdString());

            // inject dependencies
            InjectMembers(obj, ctx, reg);

            // postpone all app level [OnActivated] callbacks until all dependencies are resolved, i.e. InjectMembers() must completed for
            // all dependencies
            activate.Add(() =>
            {
               var m = Metadata.ResolveOnActivated(obj.GetType());
               if (m == null) return;
               Log.Verbose("Calling [OnActivated] {0}.{1}()", obj.GetType().Name, m.Name);
               m.Invoke(obj, Array.Empty<object>());
            });

            // if this is top level (root) object we're resolving, fire [OnActivated] callbacks
            if (isRoot) activate.ForEach(x => x.Invoke());
         }
         finally
         {
            if (isRoot) activate.Clear();
         }

         // all of the above should be done in OnActivating() but cyclic deps will fail there
         // 1) in constructor injection, cyclic dependencies are fatal, unless handled by proxies
         // 2) in module/module dependencies, cycles are anti-pattern
         // 3) in class/class dependencies, cycles are pretty normal and should be supported.
         //    typical example is parent/children relationship
         // that said, for standard use cases, cyclic dependencies between DI components should be minimal, if any, but they definitely may
         // happen, and DI should support them
      }

      private static void OnRelease(object instance)
      {
         Log.Debug("OnRelease() {0}", instance.ToInstanceIdString());
         (instance as IDisposable)?.Dispose();
      }
   }
}
