using System;
using System.Reflection;
using Autofac;

namespace JRevolt.Injection
{
   /// <summary>
   ///
   /// </summary>
   public static class InjectionExtensions
   {
      /// <summary>
      ///
      /// </summary>
      /// <param name="builder"></param>
      /// <param name="assemblies"></param>
      /// <returns></returns>
      public static ContainerBuilder UseInjector(this ContainerBuilder builder, params Assembly[] assemblies)
      {
         var metadata = new MetadataBuilder().UseAssembly(assemblies).Build();
         return builder.UseInjector(metadata);
      }

      /// <summary>
      ///
      /// </summary>
      /// <param name="builder"></param>
      /// <param name="types"></param>
      /// <returns></returns>
      public static ContainerBuilder UseInjector(this ContainerBuilder builder, params Type[] types)
      {
         var metadata = new MetadataBuilder().UseType(types).Build();
         return builder.UseInjector(metadata);
      }

      /// <summary>
      ///
      /// </summary>
      /// <param name="builder"></param>
      /// <param name="metadata"></param>
      /// <param name="onRegistration"></param>
      /// <returns></returns>
      public static ContainerBuilder UseInjector(this ContainerBuilder builder, Metadata metadata, Injector.ProcessRegistration? onRegistration = null)
      {
         new Injector(metadata).Register(builder, onRegistration);
         builder.RegisterBuildCallback(x => x.Resolve<Injector>().OnBeginLifetimeScope(x));
         return builder;
      }

      /// <summary>
      ///
      /// </summary>
      /// <param name="container"></param>
      /// <param name="scopeProvider"></param>
      /// <param name="action"></param>
      /// <typeparam name="T"></typeparam>
      public static void Run<T>(this IContainer container, Func<IContainer,ILifetimeScope> scopeProvider, Action<T> action) where T : class
      {
         using var scope = scopeProvider(container);
         action.Invoke(scope.Resolve<T>());
      }

      /// <summary>
      ///
      /// </summary>
      /// <param name="container"></param>
      /// <param name="action"></param>
      /// <typeparam name="T"></typeparam>
      public static void Run<T>(this IContainer container, Action<T> action) where T : class
      {
         container.Run(c=>c.BeginLifetimeScope(), action);
      }

      /// <summary>
      ///
      /// </summary>
      /// <param name="container"></param>
      /// <param name="action"></param>
      public static void Run(this IContainer container, Action<IContainer> action)
      {
         action(container);
      }
   }
}
