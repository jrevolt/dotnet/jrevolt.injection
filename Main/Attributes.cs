using System;
using Autofac.AttributeExtensions;
using JetBrains.Annotations;

namespace JRevolt.Injection
{
   /// <summary>
   ///
   /// </summary>
   [AttributeUsage(AttributeTargets.Class|AttributeTargets.Interface)]
   [MeansImplicitUse]
   public class ServiceAttribute : RegistrationAttribute
   {
      public ServiceAttribute() : base(Lifestyle.SingleInstance)
      {
      }
   }

   /// <summary>
   ///
   /// </summary>
   [AttributeUsage(AttributeTargets.Class)]
   [MeansImplicitUse]
   public class ComponentAttribute : RegistrationAttribute
   {
      public ComponentAttribute() : base(Lifestyle.InstancePerLifetimeScope)
      {
      }
   }

   /// <inheritdoc />
   /// <summary>
   /// </summary>
   [AttributeUsage(AttributeTargets.Property|AttributeTargets.Field)]
   [MeansImplicitUse]
   public class InjectAttribute : Attribute
   {
      /// <inheritdoc />
      /// <summary>
      /// </summary>
      /// <param name="name"></param>
      public InjectAttribute(string? name = null) => Name = name!;

      /// <summary>
      ///
      /// </summary>
      public string? Name { get; }
   }

   /// <inheritdoc />
   /// <summary>
   /// OnActivated() callback
   /// </summary>
   [AttributeUsage(AttributeTargets.Method)]
   [MeansImplicitUse]
   public class OnActivatedAttribute : Attribute
   {
   }
}
