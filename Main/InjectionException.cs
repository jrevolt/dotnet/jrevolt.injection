using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace JRevolt.Injection
{
   [Serializable]
   public class InjectionException : ApplicationException
   {
      [ExcludeFromCodeCoverage]
      public InjectionException(string? message, Exception? innerException = null) : base(message, innerException)
      {
      }

      [ExcludeFromCodeCoverage]
      protected InjectionException([JetBrains.Annotations.NotNull] SerializationInfo info, StreamingContext context) : base(info, context)
      {
      }
   }
}
