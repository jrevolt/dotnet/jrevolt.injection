using System.Reflection;
using Autofac;
using Autofac.Core;

namespace JRevolt.Injection
{
   public interface IDynamicServiceResolver
   {
      void Resolve(object target, IComponentContext ctx, IComponentRegistration registration, MemberInfo member, out object result);
   }

   public interface IDynamicServiceResolver<T> : IDynamicServiceResolver where T : class
   {
   }
}
