﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Autofac.AttributeExtensions;
using Autofac.Core;
using Serilog;
using Serilog.Events;

namespace JRevolt.Injection.TestConsoleApp
{
   internal abstract class Program
   {
      internal static void Main()
      {
         Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .MinimumLevel.Override("JRevolt.Injection", LogEventLevel.Verbose)
            .MinimumLevel.Override("JRevolt.Injection.TestConsoleApp", LogEventLevel.Debug)
            .Enrich.WithThreadId()
            .WriteTo.Console(outputTemplate: "{Timestamp:HH:mm:ss.fff}|{Level:u3}|{ThreadId}|{SourceContext}| {Message:lj}{NewLine}{Exception}")
            .CreateLogger();

         var meta = new Metadata(Assembly.GetExecutingAssembly());
         var container = new ContainerBuilder()
            .UseInjector(meta)
            .Build();

         using (container)
         {
//            container.BeginLifetimeScope().Resolve<IService>().Run();
//            container.BeginLifetimeScope().Resolve<IService>().Run();

//            Task.Run(() => { using (var scope = container.BeginLifetimeScope()) scope.Resolve<IService>().Run(); }).Wait();
//            Task.Run(() => { using (var scope = container.BeginLifetimeScope()) scope.Resolve<IService>().Run(); }).Wait();

            //Task.Run(() => RunTask(container)).Wait();

            Task.WaitAll(new Task[]
            {
               Task.Run(() => RunTask(container)),
               Task.Run(() => RunTask(container)),
               Task.Run(() => RunTask(container)),
            });
         }
      }

      static void RunTask(IContainer container)
      {
         try
         {
            using var scope = container.BeginLifetimeScope();
            scope.Resolve<IService>().Run();
         }
         catch (Exception e)
         {
            Log.Error(e, "damn!");
         }
      }
   }

   [SingleInstance]
   public class LoggerResolver : IDynamicServiceResolver<ILogger>
   {
      public void Resolve(object target, IComponentContext ctx, IComponentRegistration registration, MemberInfo member, out object result)
      {
         result = Log.ForContext(registration.Activator.LimitType);
      }
   }

}
