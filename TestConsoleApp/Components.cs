using System;
using System.Threading;
using Autofac;
using Autofac.AttributeExtensions;
using Serilog;

namespace mynamespace
{
   public interface IService
   {
   }

   [SingleInstance]
   internal class MyService : IService
   {
   }
}

namespace JRevolt.Injection.TestConsoleApp
{
   public class BaseComponent : IDisposable
   {
//      [Inject]
      protected ILogger Log { get; } = Serilog.Log.ForContext<BaseComponent>();

      [OnActivated]
      protected void OnActivated() => (Log ?? Serilog.Log.Logger).Debug("OnActivated() : {0}@{1}", GetType(), GetHashCode());

      public void Dispose() => (Log ?? Serilog.Log.Logger).Debug("Dispose() : {0}@{1}", GetType(), GetHashCode());
   }

   internal interface IService
   {
      void Run();
   }

   [SingleInstance]
//   [InstancePerLifetimeScope]
   internal class MyService : BaseComponent, IService
   {
      [Inject] private IComponent Component { get; }

      public void Run()
      {
         Log.Information("MyService.Run() : Component={0}@{1}", Component?.GetType(), Component?.GetHashCode());
         Component?.Run();
//         Benchmark();
      }

      private void Benchmark()
      {
         var iterations = 20000;

         var c = Injector.Scope.Resolve<IComponent>();
         var since = DateTime.Now;
         for (var i=0; i<iterations; i++) c.Run();
         var directCall = DateTime.Now - since;

         since = DateTime.Now;
         c = Component;
         for (var i=0; i<iterations; i++) c.Run();
         var proxiedCall = DateTime.Now - since;

         Log.Information("direct:  {0} ({1}x)", directCall, iterations);
         Log.Information("proxied: {0} ({1}x)", proxiedCall, iterations);
         Log.Information("Overhead: {0}", 1-directCall/proxiedCall);
      }
   }

   internal interface IComponent
   {
      void Run();
   }

   [InstancePerLifetimeScope]
   internal class MyComponent : BaseComponent, IComponent
   {
      internal TimeSpan now = DateTime.Now.TimeOfDay;

      public virtual void Run()
      {
         //Log.Information("MyComponent.Run() @{0}, now={1}", GetHashCode(), now);
         for (var since = DateTime.Now; (DateTime.Now - since).TotalMilliseconds < 0.1;) Thread.Sleep(TimeSpan.FromTicks(1));
      }
   }

}
