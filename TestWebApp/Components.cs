using System;
using System.Threading.Tasks;
using Autofac.AttributeExtensions;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace JRevolt.Injection.TestWebApp
{
   [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
   public class ControllerScopeAttribute : InstancePerLifetimeScopeAttribute
   {
   }

   [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
   public class ServiceScopeAttribute : SingleInstanceAttribute
   {
   }

   [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
   public class DataRepositoryScopeAttribute : SingleInstanceAttribute
   {
   }

   [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
   public class RequestScopeAttribute : InstancePerRequestAttribute
   {
   }

   public class LifecycleEvents : IDisposable
   {
      [Inject] protected ILogger Log { get; }
      [OnActivated] internal void OnActivated() => Log.Information("Activated {0}", GetType().Name);
      public void Dispose() => Log.Information("Disposed {0}", GetType().Name);
   }

   [ControllerScope]
   public abstract class BaseController : LifecycleEvents
   {
   }

   [ServiceScope]
   public abstract class BaseService : LifecycleEvents
   {
   }

   [DataRepositoryScope]
   public abstract class BaseRepo : LifecycleEvents
   {
   }

   [RequestScope]
   public abstract class RequestComponent : LifecycleEvents
   {
   }

   //

   [ApiController]
   [Route("api/[controller]")]
   public class VersionController : BaseController
   {
      [Inject] private IVersionService VersionService { get; }

      [HttpGet]
      public async Task<string> Get()
      {
         var scope = Injector.Scope;
         await Task.WhenAll(new []
         {
            Task.Run(() => { }),
            // Task.Run(() => { using (scope.BeginLifetimeScope()) VersionService.GetVersion(); }),
            // Task.Run(() => { using (scope.BeginLifetimeScope()) VersionService.GetVersion(); }),
            // Task.Run(() => { using (Injector.Scope.BeginLifetimeScope()) VersionService.GetVersion(); }),
            // Task.Run(() => { using (Injector.Scope.BeginLifetimeScope()) VersionService.GetVersion(); }),
            // Task.Run(() => { VersionService.GetVersion(); }),
            // Task.Run(() => { VersionService.GetVersion(); }),
         });

         //VersionService.GetVersion();
         // using (scope.BeginLifetimeScope()) VersionService.GetVersion();
         // using (scope.BeginLifetimeScope()) VersionService.GetVersion();
         return VersionService.GetVersion();
         // return "QDH";
      }
   }

   //

   public interface IVersionService
   {
      string GetVersion();
   }

   public class VersionService : BaseService, IVersionService
   {
      [Inject] private IVersionRepo VersionRepo { get; }

      public string GetVersion() => VersionRepo.GetVersion();
   }

   //

   internal interface IVersionRepo
   {
      string GetVersion();
   }

   public class VersionRepo : BaseRepo, IVersionRepo
   {
      [Inject] private IConnection Connection { get; }

      public string GetVersion() => Connection.GetString();
   }

   //

   internal interface IConnection
   {
      string GetString();
   }

   public class Connection : RequestComponent, IConnection
   {
      public string GetString()
      {
         Log.Information("Connection.GetString() {0}", GetHashCode());
         return GetHashCode().ToString();
      }
   }
}
