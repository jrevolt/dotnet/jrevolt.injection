﻿﻿using System;
 using System.Reflection;
 using Autofac;
 using Autofac.AttributeExtensions;
 using Autofac.Core;
 using Autofac.Extensions.DependencyInjection;
 using JRevolt.Injection.AspNetCore;
 using Microsoft.AspNetCore.Builder;
 using Microsoft.Extensions.DependencyInjection;
 using Serilog;

 namespace JRevolt.Injection.TestWebApp
{
   public class Startup
   {
      public void ConfigureServices(IServiceCollection services) => services.AddInjectorServices();

      public void Configure(IApplicationBuilder application) => application.UseInjector();
   }

   [SingleInstance]
   public class LoggerResolver : IDynamicServiceResolver<ILogger>
   {
      public void Resolve(object target, IComponentContext ctx, IComponentRegistration registration, MemberInfo member, out object result)
      {
         result = Log.ForContext(registration.Activator.LimitType);
      }
   }

}
