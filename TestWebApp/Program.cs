﻿﻿using System.Diagnostics.CodeAnalysis;
 using JRevolt.Injection.AspNetCore;
 using Microsoft.AspNetCore.Hosting;
 using Microsoft.Extensions.Hosting;
 using Serilog;
 using Serilog.Events;

 namespace JRevolt.Injection.TestWebApp
{
   internal abstract class Program
   {
      [ExcludeFromCodeCoverage]
      internal static void Main()
      {
         Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Information()
//            .MinimumLevel.Override("Microsoft.AspNetCore.Hosting.Internal.WebHost", LogEventLevel.Information)
            .MinimumLevel.Override("Microsoft.AspNetCore.DataProtection.KeyManagement.XmlKeyManager", LogEventLevel.Error)
            .MinimumLevel.Override("JRevolt.Injection", LogEventLevel.Debug)
            // .MinimumLevel.Override("JRevolt.Injection.Injector+Locking", LogEventLevel.Verbose)
//            .MinimumLevel.Override("JRevolt.Injection.TestWebApp", LogEventLevel.Information)
            // .Enrich.WithThreadId()
            .WriteTo.Console(outputTemplate:"[{Timestamp:HH:mm:ss}|{Level:u3}|{SourceContext}] {Message:lj}{NewLine}{Exception}")
            // .WriteTo.File(path:"../out.log", outputTemplate:"{Timestamp:HH:mm:ss.fff}|{Level:u3}|{ThreadId}|{Scope}|{SourceContext}| {Message:lj}{NewLine}{Exception}")
            .CreateLogger();

         CreateHostBuilder<Startup>().Build().Run();
      }

      internal static IHostBuilder CreateHostBuilder<T>() where T : class
      {
         return Host.CreateDefaultBuilder()
            .UseServiceProviderFactory(new InjectorServiceProviderFactory())
            .ConfigureWebHostDefaults(builder => builder
               //.UseUrls("http://+:8080")
               .UseSerilog()
               .UseStartup<T>()
            );
      }
   }
}
