// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable ArrangeTypeModifiers
// ReSharper disable UnusedAutoPropertyAccessor.Global

using Autofac.AttributeExtensions;
using JetBrains.Annotations;

namespace JRevolt.Injection.Test
{

   [MeansImplicitUse, UsedImplicitly]
   public class SingletonScopeAttribute : RegistrationAttribute
   {
      public SingletonScopeAttribute() : base(Lifestyle.SingleInstance)
      {
      }
   }

   [MeansImplicitUse, UsedImplicitly]
   public class RequestScopeAttribute : RegistrationAttribute
   {
      internal const string RequestScopeTag = "RequestScope";

      public RequestScopeAttribute() : base(Lifestyle.InstancePerLifetimeScope)
      {
         LifetimeScopeTags = new object[] {RequestScopeTag};
      }
   }

   //-

   class NoService
   {
   }

   interface IService
   {
   }

   [Service]
   class MyServiceA : IService
   {
   }

   [Service]
   class MyServiceB
   {
      [Inject] internal MyServiceA ServiceField;
      [Inject] internal MyServiceA ServiceProperty { get; set; }
   }

   [Service]
   class CircularA
   {
      [Inject] internal CircularB CircularB;
   }

   [Service]
   class CircularB
   {
      [Inject] internal CircularA CircularA;
   }

   interface IComponent
   {
   }

   [Component]
   class MyComponent : IComponent
   {
   }

   [Component(Name = "MyNamedComponent")]
   class MyNamedComponent : MyComponent
   {
   }

   [Component]
   class MyNamedComponentClient : IComponent
   {
      [Inject("MyNamedComponent")] internal IComponent Field;
   }

   [Component(Name = "MyNamedComponent")]
   class MyNamedComponentWithDuplicateName
   {
   }

   class BaseWithPrivateField
   {
      #pragma warning disable 649
      [Inject] private MyComponent field;
      #pragma warning restore 649

      // ReSharper disable once ConvertToAutoProperty
      internal MyComponent Field => field;
   }

   [Component]
   class Extended : BaseWithPrivateField
   {
   }

   //


   interface IRequestScopeService
   {
      string GetString();
   }

   
   
   [RequestScope]
   class RequestScopeService : IRequestScopeService
   {
      public string GetString() => GetHashCode().ToString();
   }

   [Service]
   class SingletonWithRequestScopeDependency
   {
      [Inject] internal IRequestScopeService RequestScopeService;
   }


   //- InstancePerDependency

   interface IDependency
   {
   }

   [InstancePerDependency]
   class Dependency : IDependency
   {
   }

   [SingleInstance]
   class DependencyOwnerService
   {
      [Inject] internal IDependency Dependency;
   }

   //

   [SingleInstance]
   class ReadOnlyAutoProperty
   {
      [Inject] private IService Value { get; }

      internal IService GetValue() => Value;

      // coverage for alternative lookup path
      private IService Value2 => null;
   }

}
