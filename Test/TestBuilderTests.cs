using FluentAssertions;
using JRevolt.Injection.Testing;
using NUnit.Framework;

namespace JRevolt.Injection.Test
{
   [EnableInjection]
   public class TestBuilderTests : TestBase
   {
      [Inject] private IService MyService { get; }

      [Test]
      public void BuildNoInjection()
      {
         using var ctx = new TestBuilder().Build();
         ctx.Should().NotBeNull();
      }

      [Test]
      public void BuildWithInjection()
      {
         using (new TestBuilder().WithComponent<MyServiceA>().Build(this))
         {
            MyService.Should().NotBeNull();
         }
      }
   }
}
