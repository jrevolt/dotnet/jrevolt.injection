using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Autofac.AttributeExtensions;
using Autofac.Core;
using FluentAssertions;
using NUnit.Framework;

namespace JRevolt.Injection.Test
{
   public class InjectionExtensionsTests : TestBase
   {
      [Test]
      public void UseInjector()
      {
         var type = typeof(InjectionExtensionsTests);
         var metadata = new Metadata(Assembly.GetExecutingAssembly());
         new ContainerBuilder().UseInjector(metadata).Should().NotBeNull();
         new ContainerBuilder().UseInjector(type.Assembly).Should().NotBeNull();
         new ContainerBuilder().UseInjector(type).Should().NotBeNull();
      }
   }

   public class SimpleTests : TestBase
   {
      [Test]
      public void GetAllDeclaredFieldsAndProperties()
      {
         var list = typeof(Level3).GetAllDeclaredFieldsAndProperties().ToList();
         list.Should().NotBeNull();
         list.Count.Should().Be(4);
      }

      [Test]
      public void FindBackingField()
      {
         var t = typeof(ReadOnlyAutoProperty);
         const BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;

         var pi = t.GetProperty("Value", flags);
         var fi = Metadata.ResolveReadOnlyPropertyBackingField(pi);
         fi.Should().NotBeNull();
         fi.Should().NotBeSameAs(pi);

         // coverage for alternative lookup path
         Metadata.ResolveReadOnlyPropertyBackingField(t.GetProperty("Value2", flags)).Should().BeNull();
      }

      [Test]
      public void TestLifetimeScopeProxy()
      {
         var before = Injector.Scope;
         using (var container = new ContainerBuilder().UseInjector(Metadata.Empty).Build())
         {
            Injector.Container.Should().Be(container);
            Injector.Scope.Should().Be(container.Resolve<ILifetimeScope>());
            var root = Injector.Scope;
            root.Tag.Should().Be((root as ISharingLifetimeScope)?.RootLifetimeScope.Tag);
            using (var scope1 = container.BeginLifetimeScope())
            {
               Injector.Scope.Should().Be(scope1);
               using (var scope2 = scope1.BeginLifetimeScope())
               {
                  Injector.Scope.Should().Be(scope2);
               }
               Injector.Scope.Should().Be(scope1);
            }
            Injector.Scope.Should().Be(root);
         }
         Injector.Scope.Should().Be(before);
      }

      [Test]
      public void TestLifetimeScopeProxy2()
      {
         using var container = new ContainerBuilder()
            .UseInjector(typeof(Scope1Service), typeof(Scope2Service))
            .Build();
         Assert.Throws<DependencyResolutionException>(() => container.Resolve<Scope1Service>());
         using var scope1 = container.BeginLifetimeScope(Scope.Scope1);
         scope1.Resolve<IScope1Service>().Should().NotBeNull();
         using var scope2 = scope1.BeginLifetimeScope(Scope.Scope2);
         scope2.Resolve<IScope1Service>().Should().NotBeNull();
         scope2.Resolve<IScope1Service>().Scope2Service().Should().NotBeNull();
         var s1 = scope2.Resolve<IScope1Service>();
         s1.Scope2Service().Should().NotBeNull();
         s1.Scope2Service().Scope1Service().Should().NotBeNull();
         Task.WaitAll(new [] {
            Task.Run(() => { }),
            Task.Run(() => s1.Scope2Service().Scope1Service().Should().NotBeNull()),
            Task.Run(() => s1.Scope2Service().Scope1Service().Should().NotBeNull()),
         });
         scope2.Resolve<IScope2Service>().Should().NotBeNull();
      }

      [Test]
      public void Unresolved()
      {
         using var scope = new ContainerBuilder().UseInjector(typeof(TestService)).Build();
         Assert.Throws<DependencyResolutionException>(() => scope.Resolve<ITestService>());
      }

      [Test]
      public void ExtensionsRun()
      {
         void Noop() => Expression.Empty();

         var builder = new ContainerBuilder();
         builder.RegisterType<MyComponent>();
         var container = builder.Build();

         container.Run(_ => Noop());
         container.Run<MyComponent>(_ => Noop());
         container.Run<MyComponent>(c => c.BeginLifetimeScope(), (_) => Noop());
      }
   }

#pragma warning disable 169

   //ReSharper disable all
   #pragma warning disable 649
   internal class Level1
   {
      private string String;
      protected string ProtectedString;
   }
   #pragma warning restore 649

   internal class Level2 : Level1
   {
      private string String;
   }

   internal class Level3 : Level2
   {
      private string String;
   }

#pragma warning restore 169

   //-

   internal enum Scope
   {
      Scope1, Scope2
   }

   internal interface IScope1Service
   {
      string Hey();
      IScope2Service Scope2Service();
   }

   internal interface IScope2Service
   {
      string Hey();
      IScope1Service Scope1Service();
   }

   [InstancePerLifetimeScope(Scope.Scope1)]
   internal class Scope1Service : IScope1Service
   {
      [Inject] internal IScope2Service Dependency { get; }

      public string Hey() => "Scope1:Hey!";
      public IScope2Service Scope2Service() => Dependency;
   }

   [InstancePerLifetimeScope(Scope.Scope2)]
   internal class Scope2Service : IScope2Service
   {
      [Inject] internal IScope1Service Dependency { get; }
      public string Hey() => "Scope2:Hey!";
      public IScope1Service Scope1Service() => Dependency;
   }
}
