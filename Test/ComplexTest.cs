using System;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Autofac.AttributeExtensions;
using FluentAssertions;
using NUnit.Framework;
using Serilog;

namespace JRevolt.Injection.Test
{
   public class ComplexTest : TestBase
   {
      [Test]
      public void ComplexTest1()
      {
         var meta = new Metadata(Assembly.GetExecutingAssembly());
         using var container = new ContainerBuilder().UseInjector(meta).Build();
//         using var scope = container.Resolve<ILifetimeScope>();
         using var scope = container.BeginLifetimeScope();
//         scope.Resolve<ICTService>().Get();
//         scope.Resolve<ICTService>().Get();
//         var ctl = scope.Resolve<ICTController>();
//         ctl.Get().Should().BeSameAs("hey!");
         var tasks = new Task[10];
         for (var i = 0; i < tasks.Length; i++)
         {
            tasks[i] = Task.Run(() =>
            {
//               using var x = Injector.Scope.BeginLifetimeScope();
//               x.Resolve<ICTController>().Get().Should().BeSameAs("hey!");
//               ctl.Get().Should().BeSameAs("hey!");
               try
               {
                  Injector.Scope.Resolve<ICTController>().Get().Should().BeSameAs("hey!");
//                  Injector.Scope.Resolve<ICTService>().Get();
               }
               catch (Exception e)
               {
                  Log.Error(e.ToString());
                  //throw;
               }
            });
         }
         Task.WaitAll(tasks);
      }
   }

   interface ICTController
   {
      string Get();
   }

   interface ICTService
   {
      string Get();
   }

   [InstancePerLifetimeScope]
   class CTController : ICTController
   {
      [Inject] private ICTService Service { get; }

      public string Get() => Service.Get();
   }

   [InstancePerLifetimeScope]
   class CTService : ICTService
   {
      public string Get() => "hey!";
   }
}
