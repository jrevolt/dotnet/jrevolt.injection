using FluentAssertions;
using NUnit.Framework;

namespace JRevolt.Injection.Test
{
   public class ScopedServiceProxyTest : TestBase
   {
      [Test]
      public void Test()
      {
         var proxy = new Injector.ScopedServiceProxy(new InjectAttribute(), typeof(ITestService));
         proxy.Type.Should().Be(typeof(ITestService));
         proxy.Inject.Should().NotBeNull();
      }
   }
}
