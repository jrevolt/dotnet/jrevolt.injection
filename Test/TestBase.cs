using NUnit.Framework;
using Serilog;
using Serilog.Events;

namespace JRevolt.Injection.Test
{
   public abstract class TestBase
   {
      [OneTimeSetUp]
      public void ConfigureLogging()
      {
         Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Override("JRevolt.Injection", LogEventLevel.Information)
            .WriteTo.NUnitOutput(
               outputTemplate:"{Timestamp:HH:mm:ss.f}|{Level:u3}|{ThreadId}| {Message:lj}{NewLine}{Exception}",
               restrictedToMinimumLevel:LogEventLevel.Error)
            .CreateLogger();
      }

   }
}
