using FluentAssertions;
using JRevolt.Injection.Testing;
using JRevolt.Injection.TestWebApp;
using NUnit.Framework;

namespace JRevolt.Injection.Test
{
   public class Tests : TestBase
   {
      [Test]
      public void Third()
      {
         const string expected = "1.0.0-test.1";
         new TestBuilder()
            .WithComponent<VersionController>()
            .WithMock<IVersionService>(mock => mock.Setup(x=>x.GetVersion()).Returns(expected))
            .Run<VersionController>((_,ctl) => ctl.Get().GetAwaiter().GetResult().Should().Be(expected));
      }
   }
}
