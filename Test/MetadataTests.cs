using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Autofac.AttributeExtensions;
using FluentAssertions;
using JRevolt.Injection.TestInvalidAssembly;
using NUnit.Framework;

namespace JRevolt.Injection.Test
{
   public class MetadataTests : TestBase
   {

      [Test]
      public void Scan()
      {
         var metadata = new Metadata(typeof(TypeA), typeof(TypeB));
         metadata.GetInjections(typeof(TypeA)).Count().Should().Be(1);
         metadata.GetInjections(typeof(TypeB)).Count().Should().Be(1);
      }

      [Test]
      public void ReadOnlyAutoProperty()
      {
         var type = typeof(ReadOnlyAutoProperty);
         const BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
         var pi = type.GetProperties(flags).First();
         var fi = type.GetFields(flags).First();
         var meta = new Metadata(type, typeof(IService));
         meta.GetInjections(type).Contains(pi).Should().BeTrue();
         meta.ResolveMemberWriter(pi).Should().Be(fi);
      }

      [Test]
      public void ReadOnlyAutoPropertyFail()
      {
         var ex = Assert.Throws<ArgumentNullException>(()
            => new Metadata(typeof(ReadOnlyAutoPropertyFail), typeof(IService)));
         ex.Message.Should().Contain("Property is not writable");
      }

      [Test]
      public void ResolveMemberWriter_Invalid()
      {
         var constructor = typeof(string).GetConstructor(new[] {typeof(char[])});
         Assert.Throws<ArgumentException>(() =>
         {
            new MetadataBuilder().Build().ResolveMemberWriter(constructor);
         }).Message.Should().Contain("unexpected member type");
      }

      [Test]
      public void OnActivated()
      {
         var type = typeof(OnActivatedSample);
         new Metadata(type).ResolveOnActivated(type).Should().NotBeNull();
      }

      [Test]
      public void OnActivatedExtended()
      {
         var type = typeof(OnActivatedSampleExtended);
         new Metadata(type).ResolveOnActivated(type).Should().NotBeNull();
      }

      [Test]
      public void OnActivatedInvalid()
      {
         var ex = Assert.Throws<ArgumentException>(() =>
         {
            var type = typeof(OnActivatedSampleInvalid);
            new Metadata(type).ResolveOnActivated(type);
         });
         ex.Message.Should().Contain("Multiple [OnActivated]");
      }

      internal abstract class Base
      {
         [Inject] protected string ProtectedString { get; private set; }
      }

      [InstancePerLifetimeScope]
      internal class TypeA : Base
      {
      }

      [InstancePerLifetimeScope]
      internal class TypeB : Base
      {
      }

      [InstancePerLifetimeScope]
      [ExcludeFromCodeCoverage]
      internal class OnActivatedSample
      {
         [OnActivated]
         private void OnActivated() => Expression.Empty();
      }


   }

}
