using System;
using System.Threading.Tasks;
using FluentAssertions;
using JRevolt.Injection.TestWebApp;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Hosting;
using NUnit.Framework;

namespace JRevolt.Injection.Test
{
   /// <inheritdoc />
   /// <summary>
   /// </summary>
   // ReSharper disable once ClassNeverInstantiated.Global
   public class CustomWebAppFactory : WebApplicationFactory<Startup>
   {
      protected override IHostBuilder CreateHostBuilder() => Program.CreateHostBuilder<Startup>();
   }

   public class IntegrationTests : TestBase, IDisposable
   {
      private CustomWebAppFactory factory;

      [OneTimeSetUp]
      public void Initialize() => factory = new CustomWebAppFactory();

      public void Dispose() => factory.Dispose();

      [Test]
      public async Task Test1()
      {
         using (var client = factory.CreateClient())
         {
            var result = await client.GetAsync("/api/version");
            result.IsSuccessStatusCode.Should().BeTrue();
            var tasks = new Task[1];
            for (var i = 0; i < tasks.Length; i++) tasks[i] = client.GetAsync("/api/version");
            await Task.WhenAll(tasks);
            tasks.ForEach(x => x.IsCompletedSuccessfully.Should().BeTrue());
         }
      }
   }

   public class ConsoleTests
   {
      [Test]
      public void Test()
      {
         TestConsoleApp.Program.Main();
      }
   }
}
