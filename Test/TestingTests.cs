using System;
using System.Reflection;
using Autofac;
using Autofac.AttributeExtensions;
using FluentAssertions;
using JRevolt.Injection.Testing;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using Serilog;
using TestContext = JRevolt.Injection.Testing.TestContext;

namespace JRevolt.Injection.Test
{
   public class TestingTests : TestBase
   {
      [Test]
      public void Test1()
      {
         new TestBuilder()
            .WithComponent<TestService>()
            .Run<ITestService>(svc =>
            {
               svc.Should().BeOfType<TestService>();
               svc.As<TestService>().Dependency.Should().BeAssignableTo<IMocked>();
               svc.As<TestService>().Scope.Should().NotBeNull();
            });
      }

      [Test]
      public void Test1B()
      {
         new TestBuilder()
            .WithAssemblies(Assembly.GetExecutingAssembly())
            .Run<ITestService>(x => x.Should().BeOfType<TestService>());
      }

      [Test]
      public void Test2()
      {
         var builder = new TestBuilder()
            .WithComponent<TestService>()
            .WithComponent<TestDependencyLevel1>()
            .WithMock<IConfiguration>()
            .WithMock<ILogger>()
            .WithBuilderAction(x => x.Should().NotBeNull());

         void Action(ITestService svc)
         {
            svc.Should().BeOfType<TestService>();
            svc.As<TestService>().Dependency.Should().BeOfType<TestDependencyLevel1>();
            svc.Hello().Should().NotBeNull();
         }

         builder.Run(scope => Action(scope.Resolve<ITestService>()));
         builder.Run<ITestService>(Action);
      }

   }


   [EnableInjection]
   public class TestingTests2
   {
      [Inject] private ITestService TestService { get; }

      [Test]
      public void Test1()
      {
         var builder = new TestBuilder().WithComponent<TestService>();
         using (builder.Build(this))
         {
            TestService.Hello().Should().NotBeNullOrEmpty();
         }
      }

   }

   [EnableInjection]
   public class TestingTests3 : IDisposable
   {
      private TestContext ctx;

      [Inject] private ITestService TestService { get; }

      [SetUp]
      public void SetUp() => ctx = new TestBuilder()
         .WithComponent<TestService>()
         .Build(this);

      public void Dispose() => ctx?.Dispose();

      [Test]
      public void Test1()
      {
         TestService.Hello().Should().NotBeNullOrEmpty();
      }

   }

   internal interface ITestService
   {
      string Hello();
   }

   [InstancePerLifetimeScope]
   internal class TestService : ITestService
   {
      [Inject] internal ILifetimeScope Scope { get; }
      [Inject] internal TestDependencyLevel1 Dependency;
      public string Hello() => GetType().Name;
   }

   [InstancePerLifetimeScope]
   internal class TestDependencyLevel1
   {
      [Inject] internal TestDependencyLevel2 Dependency;
   }

   [InstancePerLifetimeScope]
   internal class TestDependencyLevel2
   {
   }
}
