using Autofac;
using Autofac.Core;
using Autofac.Core.Lifetime;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace JRevolt.Injection.Test
{
   public class InternalExtensionsTests
   {
      [Theory]
      public void FindMatchingScope_ILifetimeScope()
      {
         var scope = Mock.Of<ILifetimeScope>();
         Mock.Of<IComponentLifetime>().FindMatchingScope(scope).Should().Be(scope);
      }
      [Theory]
      public void FindMatchingScope_ISharingLifetimeScope()
      {
         var scope = Mock.Of<ISharingLifetimeScope>();
         var returned = Mock.Of<ISharingLifetimeScope>();
         var lifetime = new Mock<IComponentLifetime>();

         lifetime.Setup(x => x.FindScope(scope)).Returns(returned);
         lifetime.Object.FindMatchingScope(scope).Should().Be(returned);
      }
      [Theory]
      public void FindMatchingScope_MatchingScopeLifetime()
      {
         var parent = new Mock<ISharingLifetimeScope>();
         parent.Setup(x => x.Tag).Returns("tag");
         var scope = new Mock<ISharingLifetimeScope>();
         scope.Setup(x => x.ParentLifetimeScope).Returns(parent.Object);
         var lifetime = new MatchingScopeLifetime("tag");
         lifetime.FindMatchingScope(scope.Object).Should().Be(parent.Object);
      }
      [Theory]
      public void FindMatchingScope_Null()
      {
         var scope = new Mock<ISharingLifetimeScope>();
         var lifetime = new MatchingScopeLifetime("tag");
         lifetime.FindMatchingScope(scope.Object).Should().BeNull();
      }
   }
}
