using System.Threading.Tasks;
using FluentAssertions;
using JRevolt.Injection.Testing;
using NUnit.Framework;
using TestContext = JRevolt.Injection.Testing.TestContext;

namespace JRevolt.Injection.Test
{
   public class TestContextTests : TestBase
   {
      [Test]
      public async Task Coverage()
      {
         await using var ctx = new TestContext(new TestBuilder());
         ctx.Mock.Should().NotBeNull();
         ctx.Container.Should().NotBeNull();
         ctx.Scope.Should().NotBeNull();
         ctx.ComponentRegistry.Should().NotBeNull();
         ctx.Disposer.Should().NotBeNull();
         ctx.Tag.Should().NotBeNull();
         ctx.BeginLifetimeScope().Should().NotBeNull();
         ctx.BeginLifetimeScope(b => { }).Should().NotBeNull();
         ctx.BeginLifetimeScope("subscope").Should().NotBeNull();
         ctx.BeginLifetimeScope("subscope", b => { }).Should().NotBeNull();
      }
   }
}
