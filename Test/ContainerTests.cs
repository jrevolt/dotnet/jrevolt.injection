using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Autofac.Core.Registration;
using FluentAssertions;
using NUnit.Framework;
using Serilog;

namespace JRevolt.Injection.Test
{
   public class ContainerTests : TestBase, IDisposable
   {
      private readonly Metadata metadata = new (Assembly.GetExecutingAssembly());
      private ILifetimeScope container;

      private T Get<T>() => container.Resolve<T>();

      [SetUp]
      public void Setup()
      {
         container = new ContainerBuilder()
            .UseInjector(metadata)
            .Build();
      }

      public void Dispose() => container?.Dispose();

      [Test]
      public void Load()
      {
      }

      [Test]
      public void OrderBy()
      {
         Assembly.GetExecutingAssembly().GetTypes()
            .OrderBy(t => t.FullName)
            .ForEach(t => Log.Information("{0}", t));
      }

      [Test]
      public void NoService()
      {
         Assert.Throws<ComponentNotRegisteredException>(() => Get<NoService>());
      }

      [Test]
      public void LookupService()
      {
         Get<MyServiceA>().Should().NotBeNull();
      }

      [Test]
      public void LookupServiceAsInterface()
      {
         Get<IService>().Should().NotBeNull();
      }

      [Test]
      public void LookupComponent()
      {
         Get<MyComponent>().Should().NotBeNull();
      }

      [Test]
      public void LookupNamedComponent()
      {
         const string name = "MyNamedComponent";
         container.ResolveNamed<IComponent>(name)
            .Should().NotBeNull().And.BeOfType<MyNamedComponent>();
         container.ResolveNamed<MyNamedComponentWithDuplicateName>(name)
            .Should().NotBeNull().And.BeOfType<MyNamedComponentWithDuplicateName>();
      }

      [Test]
      public void InjectedField()
      {
         Get<MyServiceB>().ServiceField.Should().NotBeNull();
      }

      [Test]
      public void InjectedProperty()
      {
         Get<MyServiceB>().ServiceProperty.Should().NotBeNull();
      }

      [Test]
      public void InjectedNamedField()
      {
         var component = Get<MyNamedComponentClient>();
         component.Should().NotBeNull();
         component.Field.Should().NotBeNull();
         component.Field.Should().BeOfType<MyNamedComponent>();
      }

      [Test]
      public void InjectedPrivateFieldFromSuperClass()
      {
         var component = Get<Extended>();
         component.Should().NotBeNull();
         component.Field.Should().NotBeNull();
      }

      [Test]
      public void Circular()
      {
         //Assert.Throws<DependencyResolutionException>(Get<CircularA>);
         var a = Get<CircularA>();
         var b = Get<CircularB>();
         a.Should().NotBeNull();
         b.Should().NotBeNull();
         a.CircularB.Should().NotBeNull();
         b.CircularA.Should().NotBeNull();
      }

      [Test]
      public void Multiple()
      {
         container.Resolve<IEnumerable<IComponent>>().Count().Should().BeGreaterThan(1);
      }

      [Test]
      public void RequestScopeInSingleton()
      {
         //var x = container.Resolve<SingletonWithRequestScopeDependency>();

//         using (container.BeginLifetimeScope("Scope1"))
//         using (container.BeginLifetimeScope("Scope2"))
         using (var scope = container.BeginLifetimeScope(RequestScopeAttribute.RequestScopeTag))
         {
//            var scoped = scope.Resolve<RequestScopeService>();
//            scoped.Should().NotBeNull();
//            scoped.Should().BeOfType<RequestScopeService>();

            var singleton = scope.Resolve<SingletonWithRequestScopeDependency>();
            singleton.Should().NotBeNull();
            singleton.Should().BeOfType<SingletonWithRequestScopeDependency>();
            singleton.RequestScopeService.Should().NotBeNull();
            singleton.RequestScopeService.GetType().FullName.Should().Match("Castle.Proxies.*");
            singleton.RequestScopeService.GetString().Should().NotBeNull();
         }
      }

      [Test]
      public void InstancePerDependency()
      {
         using (var scope = container.BeginLifetimeScope())
         {
            var svc = scope.Resolve<DependencyOwnerService>();
            svc.Should().NotBeNull();
            svc.Dependency.Should().NotBeNull();
            //todo review: proxy is injected, should it?
         }
      }

      [Test]
      public void ReadOnlyAutoProperty()
      {
         using (var scope = container.BeginLifetimeScope())
         {
            var resolved = scope.Resolve<ReadOnlyAutoProperty>();
            resolved.Should().NotBeNull();
            resolved.GetValue().Should().NotBeNull();
         }
      }

      [Test]
      public void AsyncTest()
      {
         using (container.BeginLifetimeScope())
         {
            Get<DependencyOwnerService>().Should().NotBeNull();
            Task.WaitAll(Task.Run(()=>
            {
               var svc = Get<DependencyOwnerService>();
               svc.Should().NotBeNull();
               svc.Dependency.Should().NotBeNull();
            }));
         }
      }

      [Test]
      public void Assertions()
      {
         var member = GetType().GetMethod(nameof(Assertions)) as MemberInfo;
         Assert.Throws<ArgumentOutOfRangeException>(() => member.GetMemberType());
         Assert.Throws<ArgumentOutOfRangeException>(() => member.SetValue(this, null));
      }

   }
}
