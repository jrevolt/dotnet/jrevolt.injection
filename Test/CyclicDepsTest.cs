using System.Linq.Expressions;
using System.Threading.Tasks;
using Autofac;
using Autofac.AttributeExtensions;
using FluentAssertions;
using NUnit.Framework;

namespace JRevolt.Injection.Test
{
   public class CyclicDepsTest : TestBase
   {
      [Test]
      public void Test()
      {
         var meta = new Metadata(typeof(A), typeof(B), typeof(C));
         using var container = new ContainerBuilder().UseInjector(meta).Build();
         container.Resolve<A>().Should().NotBeNull();
         container.Resolve<A>().C.Should().NotBeNull();
         container.Resolve<B>().Should().NotBeNull();
         container.Resolve<B>().C.Should().NotBeNull();
         container.Resolve<C>().Should().NotBeNull();
         container.Resolve<C>().A.Should().NotBeNull();
         container.Resolve<C>().B.Should().NotBeNull();
      }

      [Test]
      public async Task Concurrent()
      {
         var meta = new Metadata(typeof(A), typeof(B), typeof(C));
         await using var container = new ContainerBuilder().UseInjector(meta).Build();
         await Task.WhenAll(new[]
         {
            Task.Run(() => container.Resolve<A>().Should().NotBeNull()),
            Task.Run(() => container.Resolve<B>().Should().NotBeNull()),
         });
      }
   }

   [SingleInstance]
   class A
   {
      [Inject] internal C C { get; }
      [OnActivated] void Activate() => Expression.Empty();
   }

   [SingleInstance]
   class B
   {
      [Inject] internal C C { get; }
      [OnActivated] void Activate() => Expression.Empty();
   }

   [SingleInstance]
   class C
   {
      [Inject] internal A A { get; }
      [Inject] internal B B { get; }

      [OnActivated] void Activate() => Expression.Empty();
   }
}
